<?php

// autoload_psr4.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'Afs\\Base\\Traits\\' => array($baseDir . '/package/afs/base/src/traits'),
    'Afs\\Base\\Controllers\\' => array($baseDir . '/package/afs/base/src/controllers'),
    'Afs\\Base\\' => array($baseDir . '/package/afs/base/src'),
);
