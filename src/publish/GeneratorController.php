<?php

namespace Afs\Base\Publish;

use Illuminate\Console\Command;
use Afs\Base\Traits\FileUtil;

class GeneratorController extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'att:controller {controller_name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Make controller';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->publishController();
    }

    private function publishController()
    {
        $templateData = FileUtil::get_template('att_controller', $this->argument('controller_name'));

        $controllerPath = app_path('Http/Controllers/');

        $fileName = str_replace('.php','',$this->argument('controller_name'));

        if (file_exists($controllerPath.$fileName.'.php')  && !FileUtil::confirmOverwrite($fileName.'.php','', $this)) {
            return;
        }

        FileUtil::createFile($controllerPath, $fileName.'.php', $templateData);

        $this->info($fileName.' created');
    }

}
