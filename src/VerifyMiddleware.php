<?php

namespace Afs\Base;

use Closure;

class VerifyMiddleware
{
  public function handle($request, Closure $next)
    {
        //check user sudah login apa belum
        if ($request->test <= 200) {
            return response()->json(['status' => 'error']);
        }

        return $next($request);
    }
}
