<?php

namespace Afs\Base\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Afs\Base\Traits\Utils;

abstract class BaseController extends Controller
{
  use Utils;

  // public function __construct(Request $request)
  // {
  //   $this->doSomething($request);
  // }

}
