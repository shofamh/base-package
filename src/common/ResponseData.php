<?php

namespace Afs\Base\Common;

class ResponseData
{

  public static function makeResponse($message, $data)
  {
      return [
        'success' => true,
        'message' => $message,
        'data'    => $data
      ];
  }

  public static function makeError($message, $data)
  {
      $resp = [
        'success' => false,
        'message' => $message
      ];

      if(!empty($data)){
        $resp['data'] = $data;
      }

      return $resp;
  }
}
