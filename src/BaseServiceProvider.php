<?php

namespace Afs\Base;

use Illuminate\Support\ServiceProvider;
use Afs\Base\GeneratorBaseCommand;
use Afs\Base\Publish\GeneratorController;

class BaseServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(TestController::class);

        $this->app->singleton('att.publish', function ($app) {
            return new GeneratorBaseCommand();
        });

        $this->app->singleton('att:controller', function ($app) {
            return new GeneratorController();
        });

        $this->commands([
            'att.publish',
            'att:controller'
        ]);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app['router']->pushMiddlewareToGroup('afs.auth', 'Afs\Base\VerifyMiddleware::class');

        $this->loadRoutesFrom(__DIR__.'/routes/api.php');
    }
}
