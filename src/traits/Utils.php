<?php

namespace Afs\Base\Traits;

use Illuminate\Http\Request;
use Afs\Base\Common\ResponseData;
use Auth;

trait Utils
{
  public function doSomething(Request $request)
  {
      if($request->isMethod('get')){
        echo 'geti ';
      }

      if($request->isMethod('post')){
        echo 'post ';
        print_r($request->all());
      }

      if($request->isMethod('put')){
        echo 'put ';
        $this->doCreate($request);
      }

      if($request->isMethod('delete')){
        echo 'delete ';
        $this->doDelete($request);
      }

      if($request->isMethod('patch')){
        echo 'patch ';
        $this->doUpdate($request);
      }
  }

  abstract public function doDelete(Request $request);
  abstract public function doCreate(Request $request);
  abstract public function doUpdate(Request $request);
}
