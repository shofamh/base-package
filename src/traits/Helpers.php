<?php
namespace Afs\Base\Traits;

if (!function_exists('get_template_file_path')) {
    /**
     * get path for template file.
     *
     * @param string $templateName
     * @param string $templateType
     *
     * @return string
     */
    function get_template_file_path($templateName)
    {
        $templateName = str_replace('.', '/', $templateName);

        $templatesPath = config(
            'infyom.laravel_generator.path.templates_dir',
            resource_path('infyom/infyom-generator-templates/')
        );

        $path = 'package/afs/base/src/templates/'.$templateName.'.stub';

        if (file_exists($path)) {
            return $path;
        }

        return base_path('vendor/afs/base/src/templates/'.$templateName.'.stub');
    }
}

if (!function_exists('get_template')) {
    /**
     * get template contents.
     *
     * @param string $templateName
     * @param string $templateType
     *
     * @return string
     */
    function get_template($templateName)
    {
        $path = get_template_file_path($templateName);

        return file_get_contents($path);
    }
}
