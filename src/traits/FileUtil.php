<?php

namespace Afs\Base\Traits;

class FileUtil
{
    public static function createFile($path, $fileName, $contents)
    {
        if (!file_exists($path)) {
            mkdir($path, 0755, true);
        }

        $path = $path.$fileName;

        file_put_contents($path, $contents);
    }

    public static function createDirectoryIfNotExist($path, $replace = false)
    {
        if (file_exists($path) && $replace) {
            rmdir($path);
        }

        if (!file_exists($path)) {
            mkdir($path, 0755, true);
        }
    }

    public static function deleteFile($path, $fileName)
    {
        if (file_exists($path.$fileName)) {
            return unlink($path.$fileName);
        }

        return false;
    }

    public static function get_template($templateName, $argumentName = null)
    {
      $contents = file_get_contents(base_path('package/afs/base/src/templates/'.$templateName.'.stub'));

      $contents = (new self)->verifyPathFolder($contents, $argumentName);

      return $contents;
    }

    public static function confirmOverwrite($fileName, $prompt = '', $command)
    {
        $prompt = (empty($prompt))
            ? $fileName.' already exists. Do you want to overwrite it? [y|N]'
            : $prompt;

        return $command->confirm($prompt, false);
    }

    protected function verifyPathFolder($contents, $argumentName)
    {
      $controllerName = explode('/', $argumentName);

      $countNamePath = count($controllerName);

      $fixName = $controllerName[$countNamePath-1];

      if($countNamePath > 1){
        $this->createDirectoryIfNotExist(app_path('Http/Controllers/').$controllerName[$countNamePath-2]);
        $contents = str_replace('$NAMESPACE_APPS$', 'App\Http\Controllers\\'.$controllerName[$countNamePath-2], $contents);
      }else{
        $contents = str_replace('$NAMESPACE_APPS$', 'App\Http\Controllers', $contents);
      }

      $contents = str_replace('$CONTROLLER_NAME$', $fixName, $contents);

      return $contents;
    }
}
