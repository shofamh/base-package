<?php

namespace Afs\Base;

use Illuminate\Console\Command;
use Afs\Base\Traits\FileUtil;

class GeneratorBaseCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'att:publish';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Make base controller';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->publishBaseController();
    }

    private function publishBaseController()
    {
        $templateData = FileUtil::get_template('att_base_controller');

        $controllerPath = app_path('Http/Controllers/');

        $fileName = 'AttBaseController.php';

        if (file_exists($controllerPath.$fileName)  && !FileUtil::confirmOverwrite($fileName,'', $this)) {
            return;
        }

        FileUtil::createFile($controllerPath, $fileName, $templateData);

        $this->info('AttBaseController created');
    }

}
